#! /usr/bin/env python2.7

import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import argparse
import os.path

fontsize = 7
width = 3.3
height = 2.3/1.3

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("figure.subplot", left=(25/72.27)/width)
pylab.rc("figure.subplot", right=(width-2/72.27)/width)
pylab.rc("figure.subplot", bottom=(26/72.27)/height)
pylab.rc("figure.subplot", top=(height-12/72.27)/height)

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def set_axis_properties(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels([])
    # ax1.set_yticks([])
    # ax1.set_ylim(0, 5.5)
    # ax1.set_xlim(0, 1)
    # ax1.set_yscale('log')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def create_figure(args) :

    fig = plt.figure(figsize=(width, height))

    ax1 = fig.add_subplot(111)
    set_axis_properties(ax1)

    records = parse_input(args.inputfile)

    plt.title('SQLite (%s)' % (args.storage),
              fontsize=fontsize)
    # plt.legend(bbox_to_anchor=(0.45, 1.6), loc="upper center", numpoints=1,
    #            ncol=3, columnspacing=0.7,
    #            labelspacing=0.1,
    #            handlelength=1,
    #            handletextpad=0.2)

    bar_width = 0.35

    benchmark_ids = list(set([r[3] for r in records]))
    benchmark_ids = sorted(benchmark_ids)

    ys = collections.defaultdict(list)
    for bench_id in benchmark_ids :
        for bench_type in ['sgxmusl', 'musl'] :
            runs = [r[5] for r in records if r[3] == bench_id and r[1] == bench_type and r[2] == args.storage]
            runs = map(float, runs)
            ys[bench_type].append(np.average(runs))

    ind = np.arange(len(benchmark_ids))

    relative_runtime = np.array(ys['sgxmusl']) / np.array(ys['musl'])
    print min(relative_runtime), max(relative_runtime)
    
    musl_rects = plt.bar(ind, ys['musl'], bar_width, color='#1f78b4', lw=0.5)
    sgxmusl_rects = plt.bar(ind+bar_width, ys['sgxmusl'], bar_width, color='#b2df8a', lw=0.5)

    # This plots the slowdown above the bars.
    for (i, rect) in enumerate(sgxmusl_rects) :
        ax1.text(rect.get_x(), # + rect.get_width() / 2,
                 rect.get_height() * 1.02,
                 '%.2f' % (ys['sgxmusl'][i] / ys['musl'][i]),
                 rotation = '90',
                 ha = 'center',
                 va = 'bottom')
    
    ax1.set_xticks(ind + bar_width)
    ax1.set_xticklabels( benchmark_ids, rotation=90 )
    ax1.set_xlim(0, len(benchmark_ids))
    
    plt.xlabel('Benchmark ID')
    plt.ylabel('Wall clock time [sec]')

    ax1.legend( (musl_rects[0], sgxmusl_rects[0]), ('musl', 'sgxmusl'), loc='upper left' )

    # plt.legend(bbox_to_anchor=(0.45, 1.6), loc="upper center", numpoints=1,
    #            ncol=3, columnspacing=0.7,
    #            labelspacing=0.1,
    #            handlelength=1,
    #            handletextpad=0.2)
    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('sqlite-%s-results-%din-%din.pdf' % (args.storage, width, height))

def parse_input(filename) :
    records = []
    f = open(filename)
    header = f.readline().strip()
    assert header == 'run;libc;storage;testno;desc;time'
    
    for line in f :
        records.append(line.strip().split(';'))

    return records

def parse_arguments():
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('--title',
                        help='figure title')
    parser.add_argument('--inputfile',
                        help='File with data to plot.')
    parser.add_argument('--storage',
                        help='memory/disk/ramdisk')
    args = parser.parse_args()
    return args

if __name__ == '__main__' :
    args = parse_arguments()
    create_figure(args)
