#! /usr/bin/env python2.7

import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import argparse
import os.path

fontsize = 7
width = 3.2
height = width/2./1.3

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(21/72.27)/height)
pylab.rc("figure.subplot", top=(height-5/72.27)/height)

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def set_axis_properties(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('bottom')
    ax1.yaxis.set_ticks_position('left')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels([])
    ax1.set_yticks([0, 40, 80, 120])
    ax1.set_ylim(0, 120)
    # ax1.set_xlim(0, 150)
    # ax1.set_yscale('log')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def create_figure(args) :

    fig = plt.figure(figsize=(width, height))

    ax1 = fig.add_subplot(111)
    set_axis_properties(ax1)

    records = parse_input(args.inputfile)
    print records
    # plt.title('SQLite', fontsize=fontsize)
    # plt.legend(loc="upper left",
    #            numpoints=1,
    #            ncol=1, columnspacing=0.7,
    #            labelspacing=0.1,
    #            handlelength=1,
    #            handletextpad=0.2)

    plt.plot([int(r[0]) for r in records],
             [int(r[1])/1024 for r in records],
             color = '#1f78b4',
             ls = '-', marker = 'o', label = type, markersize = 3)
    
    # ax1.set_xticks()
    # xlabels = ax1.set_xticklabels( map(str.upper, benchmark_names ) )
    # for (i, xlabel) in enumerate(xlabels) :
    #     if i % 2 == 0 : continue # skip even entries
    #     xlabel.set_y(-0.08)
    
    plt.xlabel('Parallel Established Connections')
    plt.ylabel('Memory [MiB]')

    # ax1.text(7500, 45, 'on-disk', ha='left', va='bottom')
    # ax1.text(7500, 15, 'in-memory', ha='left', va='bottom')
    
    # ax1.legend( (native_rects[0], sgx_rects[0]), ('musl', 'sgxmusl') )

    # leg = plt.gca().get_legend()
    # leg.draw_frame(False)
    # ltext = leg.get_texts()
    # plt.setp(ltext, fontsize=fontsize)
    # plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('connections-vs-memory-%din-%din.pdf'%(width, height))

def parse_input(filename) :
    records = []
    f = open(filename)
    
    for line in f :
        if line.startswith('#') : continue
        records.append(line.strip().split())

    return records

def parse_arguments():
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('--title',
                        help='figure title')
    parser.add_argument('--inputfile',
                        help='File with data to plot.')
    args = parser.parse_args()
    return args

if __name__ == '__main__' :
    args = parse_arguments()
    create_figure(args)
