#! /usr/bin/env python2.7

import collections
import math
import matplotlib
matplotlib.use('PDF')
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pylab
import sys
import subprocess as sp
import argparse
import os.path

fontsize = 7
width = 2.3
height = width/1.3

pylab.rc("font", family="serif")
pylab.rc("font", size=fontsize)
pylab.rc("figure.subplot", left=(30/72.27)/width)
pylab.rc("figure.subplot", right=(width-10/72.27)/width)
pylab.rc("figure.subplot", bottom=(20/72.27)/height)
pylab.rc("figure.subplot", top=(height-12/72.27)/height)

matplotlib.rcParams['ps.useafm'] = True
matplotlib.rcParams['pdf.use14corefonts'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.serif'] = 'Times'
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['lines.linewidth']=0.5
matplotlib.rcParams['axes.linewidth']=0.5

import matplotlib.pyplot as plt

def set_axis_properties(ax1) :
    # turn off ticks where there is no spine
    ax1.xaxis.set_ticks_position('none')
    ax1.yaxis.set_ticks_position('none')
    # 1.5 because there is three bars! in general: 0.5 * number of
    # bars
    # ax1.set_xticks([1024,2*1024,4*1024,6*1024])
    # ax1.set_xticklabels([])
    # ax1.set_yticks([])
    # ax1.set_ylim(0, 10000)
    # ax1.set_xlim(0, 3)
    ax1.set_yscale('log')

    # http://matplotlib.sourceforge.net/mpl_examples/pylab_examples/spine_placement_demo.py
    for loc, spine in ax1.spines.iteritems():
        if loc in ['left','bottom']:
            pass
        elif loc in ['right','top']:
            spine.set_color('none') # don't draw spine
        else:
            raise ValueError('unknown spine location: %s'%loc)

def create_figure(args) :

    fig = plt.figure(figsize=(width, height))

    ax1 = fig.add_subplot(111)
    set_axis_properties(ax1)

    records = parse_input(args.inputfile)

    plt.title('HTTP server', fontsize=fontsize)
    # plt.legend(bbox_to_anchor=(0.45, 1.6), loc="upper center", numpoints=1,
    #            ncol=3, columnspacing=0.7,
    #            labelspacing=0.1,
    #            handlelength=1,
    #            handletextpad=0.2)

    # bar_width_scaling makes bars of different figures approx the
    # same width ...
    bar_width_scaling = 4/6.
    bar_width = 0.35 * (4/6.)

    benchmark_names = list(set([r[1] for r in records]))
    benchmark_names = sorted(benchmark_names)

    resources = ['16KiB.txt', '1MiB.txt']

    for (i, resource) in enumerate(resources) :
        ys = collections.defaultdict(list)
        stdev = collections.defaultdict(list)
        for bench_name in benchmark_names :
            for bench_type in ['sgxmusl', 'musl'] :
                runs = [r[4] for r in records if r[0] == bench_type and r[1] == bench_name and r[2] == resource]
                runs = map(float, runs)
                ys[bench_type].append(np.average(runs))
                stdev[bench_type].append(np.std(runs))
                print '%s %s %.1f' % (bench_name, bench_type, (stdev[bench_type][-1] / ys[bench_type][-1]) * 100.)

        ind = (i * len(benchmark_names)) + np.arange(len(benchmark_names))
        print ys['musl'], stdev['musl']
        native_rects = plt.bar(ind, ys['musl'], bar_width, yerr = stdev['musl'],
                               color = '#1f78b4', lw = 0.5)
        print ys['sgxmusl'], stdev['sgxmusl']
        sgxmusl_rects = plt.bar(ind+bar_width, ys['sgxmusl'], bar_width, yerr = stdev['sgxmusl'],
                            color = '#b2df8a', lw = 0.5)

        for (i, rect) in enumerate(sgxmusl_rects) :
            ax1.text(rect.get_x() + rect.get_width() / 2,
                     rect.get_height() * 1.05,
                     '%.0f\\%%' % (ys['sgxmusl'][i] / ys['musl'][i] * 100.),
                     ha = 'center',
                     va = 'bottom')
    
    ax1.set_xticks(np.arange(len(benchmark_names) * len(resources)) + bar_width)
    # ax1.set_yticks([10, 100, 1000, 10000])
    xlabels = ax1.set_xticklabels( ['HTTP\n16KiB', 'HTTPS\n16KiB', 'HTTP\n1MiB', 'HTTPS\n1MiB'] )
    # for (i, xlabel) in enumerate(xlabels) :
    #     if i % 2 == 0 : continue # skip even entries
    #     xlabel.set_y(-0.08)
    
    # plt.xlabel('Protocol')
    plt.ylabel('Average Requests/Second')

    ax1.legend( (native_rects[0], sgxmusl_rects[0]), ('musl', 'sgxmusl') )

    leg = plt.gca().get_legend()
    leg.draw_frame(False)
    ltext = leg.get_texts()
    plt.setp(ltext, fontsize=fontsize)
    plt.setp(leg.get_frame(), linewidth=0.5)

    plt.savefig('www-results-%din-%din.pdf'%(width, height))

def parse_input(filename) :
    records = []
    f = open(filename)
    header = f.readline().strip()
    assert header == 'type,protocol,resource,run,average requests/second'
    
    for line in f :
        if line.startswith('#') : continue
        records.append(line.strip().split(','))

    return records

def parse_arguments():
    parser = argparse.ArgumentParser(description='.')
    parser.add_argument('--title',
                        help='figure title')
    parser.add_argument('--inputfile',
                        help='File with data to plot.')
    args = parser.parse_args()
    return args

if __name__ == '__main__' :
    args = parse_arguments()
    create_figure(args)
