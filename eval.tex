\section{Evaluation}
\label{sec:evaluation}

The evaluation presents a mix of micro and macro benchmarks that are aimed at understanding the system's overall performance.
Since we do not yet have real SGX hardware,
the measurements were performed on standard x86 servers.
We first present micro benchmarks where we quantify the application-level performance for individual components of the three-tier architecture,
as well as, a few additional applications.

Unless otherwise noted, we ran the benchmarks on servers equipped with 2 Xeon E5405 processors clocked at 2~GHz.
Each machine has 8~GiB of memory.
The machines are connected via switched Gigabit Ethernet and they run a recent version of Ubuntu (14.04.03 LTS).

\begin{figure*}
\center
\includegraphics{figs/sqlite-disk-results-3in-1in}
\hspace{0.5cm}
\includegraphics{figs/sqlite-ramdisk-results-3in-1in}
\caption{SQLite performance.
\emph{sgxmusl} performance is on-par with a standard \emph{musl} implementation.}
\label{fig:eval-micro-sqlite}
\end{figure*}

\subsection{Micro-Benchmarks}

We compile each application with the same compiler and linker flags.
Most measurements will compare two versions of the same application.
One version, labeled \emph{musl},
is built with the standard unmodified musl C library.
We link each application statically and compile for position independent code (\verb+-fPIC+).
The second version of each application, labeled \emph{sgxmusl},
is built with our modified version of the musl C library.
The build process for the \emph{sgxmusl} version produces a statically linked shared object file.
We use a custom ``starter'' application to load the shared object file.
The starter emulates the setup of an actual enclave, i.e.,
it allocates (enclave) memory and initializes certain variables (cf. Section~\ref{sec:enclave-emulation}).
The starter is independent of the actual application.
To launch a specific application,
we pass the application's shared object file as a command line parameter to the starter.

Next we present the micro-benchmark applications:

\noindent
\textbf{SSL terminator (stud)}:
stud is an open-source stand-alone SSL terminator\footnote{\url{https://github.com/bumptech/stud}}.
Often, the ability to terminate the SSL connection is also part of the HTTP server itself.
It is, however, also possible to terminate the SSL session outside of the server process.
In this way, even if the server process were compromised, we can protect the certificates of the service.
% it is may be more difficult to extract information from the application itself.
We focus on the SSL session termination time and I/O throughput.

\noindent
\textbf{LUA}:
LUA is an embeddable scripting language,
popular to allow for the easy extension and modifiability of the host application.
For example, the web servers nginx and Apache both can be customized with LUA scripts.
We performed benchmarks with 6 different applications written in LUA\footnote{\url{http://benchmarksgame.alioth.debian.org/u64q/lua.html}}.
These applications are CPU intensive with little to no I/O.
We measure the elapsed time and expect the difference between the \emph{musl} and \emph{sgxmusl} version to be small.

\noindent
\textbf{SQLite}:
``SQLite is a software library that implements a self-contained, serverless, zero-configuration,
 SQL database engine.''\footnote{\url{https://www.sqlite.org}}.
Typically, it is directly linked with an application to persistently store and retrieve data with SQL syntax.
We use SQLite as our main data store,
because it is versatile, has a relatively small code base (fewer than 10K lines of code),
is widely used in practice and intensively tested.

The SQLite source code repository includes a speedtest application,
which we use as our benchmark.
The speedtest consists of reading and writing indexed and non-indexed tables,
as well as, query execution.
For details its best to look at the source code directly\footnote{\url{https://www.sqlite.org/src/finfo?name=test/speedtest1.c}}.
We run the benchmark with an in-memory and on-disk database,
to compare the overhead of \emph{sgxmusl} with and without the actual disk I/O impact.

\noindent
\textbf{mongoose HTTP server}:
We use mongoose\footnote{\url{https://github.com/cesanta/mongoose}} as the basis for our application and database server.
\emph{Mongoose} is a web server for embedded systems,
``provides [an] easy to use event-driven interface that allows to implement network protocols or scalable network applications with little effort''.
Mongoose is written in C and consists of a single source and header file comprising about 10K lines of code (measured with \verb+wc -l+).
Independently of this,
we measured its performance in serving static files via HTTP and HTTPS.
We report the throughput for \emph{musl} and \emph{sgxmusl} versions of a mongoose web server.


\begin{figure}
\center
\includegraphics{figs/syscall-delay-2in-1in}
\caption{Adding delay to each system call increases the overall execution time accordingly.}
\label{fig:syscall-delay}
\end{figure}

\subsection{Results}

Figure~\ref{fig:eval-micro} shows the results for our micro-benchmarks with LUA, stud, and the mongoose HTTP server.

The \textbf{LUA} benchmark shows the elapsed time for 6 different CPU-bound applications.
The applications perform less than 100 system calls each.
Hence, the system call forwarding mechanism cannot be responsible for the small,
but noticeable, overhead.
In the absence of system calls,
we assume our enclave memory allocator contributes to the \emph{sgxmusl} overhead.
Recall that a chunk of memory for the enclave is allocated once at startup time.
From this point on,
the enclave manages this memory itself.
This allocator (mem5) is different from the standard musl allocator and may be partially responsible for the performance difference.
In any case, we consider the overhead acceptable and we are confident to reduce it further pending additional profiling and optimization.

For \textbf{stud}, we measured the number of new connections per second the TLS terminator could sustain.
We used a single \verb+openssl s_time+ process as a load generator running on the same machine as the server.
We close the connection immediately and no payload is transferred over the connection.
stud's \emph{sgxmusl} performance is almost identical to its \emph{musl} variant.
TLS termination is CPU-intensive as signatures have to be verified and signed.
The throughput drops with increasing key sizes,
because the complexity of the cryptographic computations increases.

For the \textbf{HTTP server}, we measured the throughput in requests per second the server could sustain.
For the first time,
the performance between the \emph{musl} and \emph{sgxmusl} differs substantially.
In addition to establishing a (secure) connection,
we also transfer data from the server to the client.
Server and client run on the same machine.
We used Apache Bench (\verb+ab+) to generator load.
The load generator repeatedly request a single static file.

We see that the overhead increases for a larger static file.
\emph{sgxmusl}  has a larger overhead for larger files since it has to copy the content of the file leading to increasing cache pollution. 
We see this particularly for the https case where the cache pressure is increasing even more and hence, see even higher slow downs. 
 
%\tk{Not how to explain why performance for HTTP/16KiB is 65\% of musl.
% Have to investigate if server is CPU-bound or if we need more clients \ldots}

For the \textbf{SQLite} speedtest benchmark,
we measured the elapsed time to execute the speedtest benchmark.
\emph{sgxmusl} took around 44.1 second to complete,
compared with 38.2 second of the \emph{musl} version.
We chose to put the database on-disk,
 as the in-memory version performed almost no system calls ($215\,K$ vs $2.6\,M$).
The biggest relative slowdown is for benchmark ID 130:
its runtime increases by 40\%.
The slowdown for the entire SQLite benchmarks is moderate with around 15\%.

We also evaluated how an increased system call latency would affect the performance.
Besides copying the arguments between enclave and non-enclave memory,
an alternative implementation might instead leave the enclave mode to perform a system call.
To emulate the additional costs for an enclave switch,
we included an additional delay for each system call.
Figure~\ref{fig:syscall-delay} shows how the SQLite benchmark's overall runtime increases with the delay.
While there is no change with an in-memory database of SQLite (almost no system calls),
the elapsed time increases proportionally to the delay if the application actually does perform system calls.

\begin{figure}[t]
\includegraphics[width=\columnwidth]{figs/evaluation-setup}
\caption{The setup of our macro benchmark.
Each component runs on a different physical server.}
\label{fig:evaluation-setup}
\end{figure}

\subsection{Macrobenchmark - Encrypted Websocket Chat}
\label{sec:webchat}

Our choice of applications to use for evaluation was limited. 
% due to the missing tool support for SGX.
%link to a static.
%This is necessary to capture all the transitions from user to kernel space, i.e.,
%to proxy all system calls from the application.
%
%
While web frameworks, for example, Django and Ruby on Rails,
use high-level scripting languages -- with all their benefits and drawbacks -- we were looking for a more lightweight alternative.
Securing an application written in a scripting language would necessarily mean to run the entire interpreter/runtime as part of the secure environment.
In our effort to keep the code base within the enclave small,
we decided against a scripted language.
Instead, we decided that the applications, as well as, all their dependencies,
should allow us to pack it in a statically linked binary.

\begin{figure}
\center
\includegraphics[width=\columnwidth]{figs/eval-throughput-macro}
\caption{Throughput and scalability with 1 to 4 application servers.}
\label{fig:eval-throughput-macro}
\end{figure}

We extended the basic WebSocket chat server that is included as an example application with the mongoose framework.
All connections are encrypted with TLS (see Figure \ref{fig:evaluation-setup}).
Clients connect to the server via HTTP, loading the main page.
Interaction between the participating chat users happens via WebSockets.
WebSockets are a low-overhead way for the client and server to bi-directionally exchange messages.
Each client connects to one of multiple chat servers,
joining a chat room, specified as part of the URL.
Each message is logged to the database,
by sending an HTTP PUT request to the database.
Upon joining a room,
the client is presented with a history of the last $N$ messages exchanged within the chat room. In our experiments we set $N$ to 20.
The application server obtains the list of messages by sending an HTTP GET request to the database.

To provide a secure persistent storage to our web service application, we adapted the popular open-source database engine SQLite.
SQLite is typically directly linked into an application.
It provides the ability to read and manipulate data with SQL commands.
The data is stored in a single local file.
%Of course, this limits scalability.
However, SQLite is especially appealing for small projects running in a resource-constrained environment, e.g.,
embedded systems or mobile platforms. Since enclaves are also resource constrained, our
choice was to use SQLite instead of full-blown SQL database like MySQL.  
%Hence, SQLite is the most widely deployed database engine.  
In case the SQLite database would become a bottleneck, one could scale up the number of databases by partitioning the data with respect to the chatroom id. 


%Because we want to access the database from multiple sources over the network,
%we require a ``front end''.
%We reused code from the mongoose project~\footnote{\url{https://github.com/cesanta/mongoose}} to access the database over a REST interface.
To allow multiple sources to access the database over the network, we developed a front end that implements a REST interface. 
The interface allows us to send HTTP requests to store and retrieve data from the SQLite database. All connections are encrypted with TLS.
Our implementation is based on the API server from the mongoose project. 
The interface itself is minimal and driven by the needs of our application.
%As will be described in more detail later (cf. Section~\ref{sec:webchat}),
%we implemented a secure webchat with persistent history.
The entire message history for each chat room is stored persistently.
That is, we perform a PUT operation for each new message.
On the other hand, when a new user logs in,
she is presented with the N most recent messages requested from the database by the application server.


Interestingly, the cloud provider has no way of tapping into the chat conversations at any point.
The communication between the different tiers is encrypted,
the in-memory state is protected by Intel's SGX technology,
and the database only stores encrypted data too.
It is possible to write a chat client based on mongoose and compile it with our modified SGX musl C library,
to also protect the client.


%% \begin{figure}
%% \includegraphics[width=3.2in]{figs/websocket-chat}
%% \caption{Picture of the webchat window.}
%% \label{fig:websocket-chat}
%% \end{figure}

%% Clients can join the chat by loading the main page and leave the chat by closing the browser window.
%% Clients can also broadcast messages: first, the message arrives at the server,
%% which in turn sends a unicast message to all other clients.
%% Each message is also logged persistently in RESTified SQLite database (cf. Section~\ref{sec:sqlite}).

\begin{figure}
\center
\includegraphics{figs/connections-vs-memory-3in-1in}
\caption{Memory usage of the application server based on the number of parallel connections.}
\label{fig:connections-vs-memory}
\end{figure}
