\section{Design and Architecture}
\label{sec:design}


One of our objective is to support a classical multi-tier architecture 
consisting of a client tier, an application tier and a data tier (see Figure~\ref{fig:three-tier-architecture}). 
In what follows, we discuss the load balancer and the application servers of the application tier and the data tier. 
We sometimes refer to an application server as a backend (server). 

\begin{figure}
%\includegraphics{figs/three-tier-architecture}
\includegraphics[width=\columnwidth]{figs/architecture}
\caption{We support a classic three tier web service architecture.
A load balancer distributes requests among a variable set of application servers.
Persistent state is stored in the database. All communication is encrypted and
application server and database run inside enclaves.}
\label{fig:three-tier-architecture}
\end{figure}

\subsection{Load Balancer}
\label{sec:load-balancer}
The clients' entry point for the secure service is the load balancer.
An internet scale service could have millions of users.
To handle high load as well as for fault tolerance purposes, multiple application servers are required.
The load balancer forwards the request to one of a number of backend servers.
Standard strategies include (weighted) round-robin, random, and hash-based, to name a few.
Most load balancer can be configured/extended to scale the number of backend servers dynamically.
Such an extension is easiest with software load balancers, such as nginx~\cite{reese2008nginx} or haproxy~\cite{tarreau2012haproxy}.
For high traffic volumes,  one can also deploy multiple software load balancers (e.g., each serving
on a different IP address) or one can deploy hardware load balancers.

To communicate securely with the service, clients connect to it over a secure channel.
In practice, this means a TCP connection secured with SSL/TLS.
Modern web servers support SSL/TLS, as do load balancers, and the clients.
The question is where to terminate the SSL connection. According to our system and failure model, 
all communication  needs to be encrypted.
In particular, we need to encrypt the traffic between the load balancer and the application servers.
Hence, if we were to terminate the SSL connection at the load balancer (which many web services do), 
we would need to reencrypt the traffic.
This would not only cost extra CPU cycles by decrypting and reencryption but also because we could not use
optimizations like TCP splicing~\cite{maltz1998tcp} which, for example, haproxy~\cite{tarreau2012haproxy} supports. 
Also, we would have to ensure that the load balancer does not become a bottleneck as terminating the SSL session
requires more computational power.

Since we terminate the secure connections at the backends, the load balancer will only handle encrypted traffic.
While the load balancer can drop the traffic, amounting to a denial of service attack, 
it cannot compromise the integrity or confidentiality of the data passing through.
As the load balancer does not access any certificates either,
it does not need to run in a secure execution environment.
This permits application developers to - instead of running a private load balancer - use offerings by the cloud provider,
like the Amazon Elastic Load Balancer, without compromising confidentiality or integrity.
% A preconfigured load balancer reduces our administration effort. 

% If the traffic between the load balancer and the application server could be unencrypted,
% terminating at the load balancer would mean less computational overhead for the backend servers, as they only send and receive plain text.
% However, we would have to ensure that the load balancer does not become a bottleneck, as terminating the SSL session requires more computational power.
% Experience with large internet services has shown, however, that with modern processors and their hardware acceleration for cryptographic primitives,
% the computational overhead of SSL/TLS is small (a few percent~\footnote{\url{https://www.imperialviolet.org/2010/06/25/overclocking-ssl.html}}).

With respect to key management,
in general, a centralized SSL terminator is easier to handle as certificates must be deployed and installed only in a single location.
Terminating the connection at the backend means spreading the keys to many backend servers,
potentially increasing the possibility that any one of them can be compromised to extract the certificates.
In our architecture, we protect the backends and their keys by running these inside enclaves. Moreover,
since we have to encrypt the traffic between all enclaves, we anyhow have to maintain keys in each enclave.
This implies that we would not reduce the key management problem by having a centralized SSL termination.

%In our architecture, we decided to terminate the SSL connection at the backend servers to improve security.
%In doing so, we keep the load balancer outside of the trusted computing base.
%The load balancer is an off-the-shelf software component, which we do not have to trust.

%Confidentiality and integrity are preserved as the load balancer only relays encrypted traffic between the client and the backend servers.
%[What about the load balancer forwarding independent requests of the same client to different backends? Will this not violate “sessions” at the application layer?]

In our architecture, it is even possible to use a single load balancer for multiple websites hosted at the same IP address.
Hosting multiple websites at the same IP address is often done due to the scarcity of IPv4 addresses.
The load balancer distinguishes HTTP requests for different websites using application layer information:
it inspects an HTTP header field to determine the correct backend for a specific HTTP request.
However, SSL/TLS operates at the transport layer, denying the load balancer access to any application layer information.

To work around this problem, Server Name Indication (SNI)~\cite{rfc6066} was developed as an extension to the standard SSL/TLS protocol.
When setting up the secure connection, the client and server perform a traditional handshake.
With SNI, the handshake also includes the clear text hostname of the website the client wishes to connect to.
By observing the handshake, the load balancer has the required information to relay the encrypted packets correctly.
SNI must be supported by the client, which is the case for a large number of modern mobile and desktop web browsers.
Popular cryptographic libraries, for example, OpenSSL, also include support for SNI.


\subsection{Backend Server}

%\subsection{Management of Certificates and Keys}

There exist quite a few frameworks to build web applications like, for example, Sinatra (Ruby), Spring (Java), and Flask (Python).
Most of these frameworks support REST services~\cite{fielding2000architectural}.
REST services use HTTP/HTTPS as an underlying transport protocol and have been quite popular in the recent years.
In our approach, we focus on supporting REST services that use HTTPS for secure transport.
 
For HTTPS, one needs to store certificates. 
The storing and processing of certificates is a sensitive area in terms of security. 
Even if the administrator of a web service configures the access rights for the certificates correctly,
this does not prevent a malicious cloud operator to dump the VM’s memory to obtain a copy of the certificates/keys.
Because certificates and keys are sensitive information, they must never leave the secure execution environment.

To protect the website certificates, we terminate the SSL/TLS connection inside enclaves only.
The SSL/TLS termination is performed by a REST framework.
The framework has access to the certificates via an encrypted configuration file:
only code that runs inside an enclave can decrypt this configuration file to access the certificates.  

The functionality provided by a REST framework is also executed inside an enclave.
For REST services that do not handle  sensitive information (except for the certificates),
one could in principle only perform the SSL/TLS termination in the enclave. 
How to partition REST services in parts that run inside and outside enclaves is
an interesting question that we plan to address in the future.
%As an alternative, we could terminated the SSL connection as part of the application server.
%However, this would put the certificates into the same secure environment as the application server.
%Bugs remaining in the application server could potentially be exploited to gain access to the SSL/TLS certificates.
%Consequently, we decided to decouple the termination of the SSL connection and the application server into two separate environments.
%??????

The backend servers running in enclaves have limited memory resources. 
Typically, the amount of memory and CPU resources needed in a backend grows proportionally to the number of connections served 
since a backend server needs to keep session state for each connection.  
We limit the resource needed by a backend server with the help of the load balancer by setting the maximum number concurrently active connections to a backend server. 
Typically, the load balancer supports horizontal scalability by helping to trigger  the spawning of  new backend servers 
before reaching the maximum number of connections on all backend servers.
Again, a load balancer can cause denial of service attacks by forwarding more than the permitted connections to a backend server or not triggering the spawning of new backends.

%The question remains how the SSL terminator communicates with the application server.
%We propose to use a lightweight communication substrate akin to a message bus.
%Individual components can send and receive message on a bus.
%This is an established design attested by the popularity of open-source frameworks, such as, ZeroMQ.
%Section X.X contains further information about the communication bus and its security.

To support horizontal scalability,  REST services typically keep their state inside of some external key/value store or 
an SQL database.

\subsection{Data Tier}
\label{sec:sqlite}

%% \begin{figure}
%% \includegraphics{figs/db-communication-flowchart}
%% \caption{.}
%% \label{fig:db-communication flowchart}
%% \end{figure}

% \tk{Mention that we use full database encryption via sqlcipher!}  - DONE!

Our primary concern when storing data is to guarantee \emph{integrity} and
\emph{confidentiality}. Intel SGX ensures these properties for the enclave
memory, however, once data leaves the secure environment, it can be read and
modified on its way to the disk and hence requires additional protection. To
that end, data must be protected with cryptographic means before being passed
to the OS, which in practice means using encryption and message authentication
codes (MAC) to ensure confidentiality and integrity. 

The database and the application servers communicate securely over TLS/SSL encrypted TCP/IP connections.
The database application consists of the mongoose web server to manage the communication over HTTP,
the SQLite library, to provide the SQL semantics and persistent storage,
and the OpenSSL library to handle the transport layer en-/decryption.
All these components run within the enclave,
thus forming a part of the trusted computing base.

Many database management systems provide users with the capability to protect confidentiality and integrity of
data at rest on the application level. 
We use sqlcipher\footnote{\url{https://github.com/sqlcipher/sqlcipher}}, an SQLite
extension to perform data encryption. The extension splits the database file in a number of pages which are
encrypted separately. 
For each page, a MAC is stored to ensure integrity. 
The key to decrypt the database is stored in an enclave and is thus protected.

We do not cover a class of attacks known as \emph{rollback attacks}.
With a rollback attack,
the integrity and confidentiality are preserved,
but an old version of the state is re-instantiated by the attacker.
In general, it is difficult for an application to detect rollback attacks in the face of crashes and restarts.
Conceptually, a non-volatile trusted counter is required to protect from rollback attacks.
The counter is incremented with each update.
After a crash or reboot, the trusted counter is compared to the version number of the durably stored data.
If the expected and actual version differ,
the system halts because it likely detected a rollback attack. 
%\sergey{Shouldn't we do that for each read?}

Protecting against rollback attacks is challenging but not impossible given today's trusted components.
Memoir~\cite{parno2011memoir} demonstrated how a practical trusted storage system can be built with existing components.
The main idea is to leverage the Trusted Platform Module~(TPM) as a trusted counter to detect rollback attacks.
This is complicated by the limited number of write cycles and low throughput of the TPM module. 
%Alternatively, the version numbers can be sent to an external trusted party. This approach, however, would increase network traffic and latency of each read and write operation.
