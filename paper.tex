\documentclass[preprint, 10pt]{sigplanconf}

\usepackage{color}
\usepackage{graphicx}
\usepackage{lipsum}
\usepackage{url}

\hyphenation{light-weight}

\begin{document}

\title{End-to-End Security for Cloud-Based Web Applications}
% \subtitle{Secure Remote Execution in Public Clouds}

\authorinfo{Paper 163}
           {N pages}
%% \authorinfo{Name2\and Name3}
%%            {Affiliation2/3}
%%            {Email2/3}

\def\sergey#1{{\color{red} #1}}
\def\tk#1{{\color{red} TK: #1}}
\maketitle

\begin{abstract}
Many companies, especially in Europe, hesitate to move their applications in public clouds. 
One of the reasons for this hesitation is that the confidentiality of data is perceived to be insufficiently protected in public clouds.
We describe and evaluate an approach to run  web applications securely  in public clouds.
This approach is based on the new Intel SGX technology which permits to run parts of an application in {\em secure enclaves}.
Our main objective is to support the confidentiality of applications with no or minimal re-engineering of their code base.
Some of the challenges one faces is that SGX limits the size of secure enclaves and one cannot issue system calls from inside of enclaves.
We ported a standard C library to support Intel SGX and show that we can set up secure cloud-based applications using common open source components:
a load balancer, an application development framework and a database.
% All components are statically linked with an open source C library,
% which we modified to run applications inside a secure execution context (enclave).
Our evaluation is performed in an emulated environment (since we will get access to real SGX hardware only later this month).
The performance overhead caused by our modifications is small.
%{\tk{The execution time overhead in our benchmarks is between X\% and Y\% of native.}}
\end{abstract}

\section{Introduction}

%% What is the problem?
Ensuring the confidentiality of data is desirable but in general difficult to achieve.  
To ensure the confidentiality of data,
we need to protect the data ``end-to-end'' since attackers will most likely try to find the weakest point where the data can be accessed. 
This implies that we need to protect the confidentiality of data at rest, during transmission and while it is being processed.
Data at rest and during transmission can be protected by encryption.
This requires, however, that we can protect the encryption keys and the plain text data while being processed.  

Traditionally, we have been constructing systems under the assumption that we can trust the hypervisor and/or the operating system kernel, i.e.,
these are part of the trusted computing base~(TCB).
Since operating systems and hypervisors are large pieces of software,
they contain vulnerabilities that can be exploited by attackers~\cite{chen2011linux,Perez-Botero:2013:CHV:2484402.2484406}.
Hence, removing the hypervisor and OS from the TCB could increase the probability that data remains confidential.

In the context of cloud computing, 
the hypervisor and depending what cloud services are used,  also the operating system will be under the control of the cloud provider.
Even if an application provider would have complete trust in the cloud provider,   
there might be  legal requirements why an application provider has to protect its data from the cloud provider:
% an application provider running services in a cloud might be under a different legislative control than the cloud provider.
A cloud provider might be under a different legislative control and might be legally required to provide access to the data of the application provider.
The application provider might be legally required to safeguard the data of the service customers even against foreign governments.
A technical way to address this issue would be to ensure that the cloud provider - even being in control of the hypervisor,
the operating system and the physical hardware - has at most access to encrypted data.

Intel's new Secure Guard Extensions (SGX) ~\cite{IntelSGXReference} have been designed to help application designers 
to build trustworthy applications in an untrusted environment by running parts of the application in {\em secure enclaves}.
Prior to that, solutions based on fully homomorphic encryption to compute on encrypted data have existed,
but are impractical for general purpose computing due to their inherently excessive overhead~\cite{gentry2011implementing}.
Now that novel hardware mechanisms exist,
it is an open question how to best use them to provide secure execution environments for legacy,
as well as, newly developed applications.
Previous work has shown how to run unmodified legacy applications in secure enclaves~\cite{baumann2014shielding}
which will potentially  bring trusted execution to thousands of already existing applications.

At least initially, the size of SGX enclaves seems to be limited to 128 MBytes\footnote{More precisely, there are reports that claim that the size 
of the enclave page cache (EPC) on mobile Skylake CPUs is limited to 128 MBytes. We expect that this limit will be raised in future CPUs.}. 
This means that one should not only reduce the amount of code that runs in an enclave  a) to reduce the number of potential vulnerabilites inside an enclave,  
but b)  to ensure that the application actually fits in an enclave.  
In this paper, we present an approach to reduce the amount of code that runs in a single enclave. 
Hence, we support spreading an application across multiple enclaves.
When building a web application, we typically split the application in a set of collaborating services that all need to be trusted. 
% Was well as communicate and persist data in a secure fashion. 
Each of these services can run in a separate enclave on a different host. 
In this way, we can circumvent some of the resource limitations as long as sufficiently many hosts in a data center support SGX. 
(Of course, the remaining resource of a host can be used by other applications that do not need access to an enclave.)

% Move the following into the related work section:
% it is not yet clear if the new trusted executions environments can really be retrofit to provide to promised guarantees~\cite{xu2015controlled-channel}.
% {\color{red} How do WE deal with that problem?}

%% Why is it interesting and important?

%% Why hasn't it been solved before? (Or, what's wrong with previous 
%% proposed solutions? How does mine differ?)
%Practical solutions to implement secure executions environments have only been proposed recently~\cite{IntelSGXReference}
%and the commercial availability of Intel SGX is expected for October 2015.
%% {\color{red} Doesn't \cite{xu2015controlled-channel} say that sgx is not good enough?}
%Prior to that, solutions based on fully homomorphic encryption to compute on encrypted data have existed,
%but were impractical for general purpose computing due to their inherently excessive overhead~\cite{gentry2011implementing}.
%Now that novel hardware mechanisms exist,
%it is an open question how to best use them to provide secure execution environments for legacy,
%as well as, newly developed applications.  Secure execution environments are  not only very useful to protect
%data processed by applications running on mobile devices but also on backend services running in public clouds.
%The focus of this paper is on how to secure cloud-based services running in public clouds.
% We specifically target the later and specifically target the class of \emph{reactive applications}~\footnote{\url{http://www.reactivemanifesto.org/}}.

%% What are the key components of my approach and results? Also
%% include any specific limitations.

% explain the problem that we address in this paper
% {\color{red} Problem addressed in this paper. }


Running common open source cloud applications in an enclave is non-trivial.  
For example, the execution of system calls is not permitted inside of an enclave since the enclave memory must be protected from the operating system. 
Haven's~\cite{baumann2014shielding} approach to deal with this problem is to pull a stripped down library OS into the enclave,
alongside the application.
Running an entire (library) OS as part of the enclave increases the TCB and, consequently,
the likelihood that this code has exploitable vulnerabilities.
Hence, our goal is to keep the (library) OS out of the TCB to decrease the exploitable attack surface
and to be able to stay below the maximum enclave size. 

%\tk{We do not yet provide any shielding, hence this paragraph is mute and should be removed.}
%In any case, even if we only provide a small set of application-specific functions within an enclave,
%we still have to ensure those functions cannot be exploited by attackers to gain access to the internal state.
%Hence, the proper partitioning of general applications in {\em shielded components} running inside of enclaves and {\em non-shielded components} running outside of enclaves is a non-trivial problem.
%Neither shielded nor non-shielded components must compromise the confidentiality of data.
To decrease the lines of code that need to run inside of enclaves, we would like to keep components
out of enclaves that only operate on encrypted data anyhow. We show how a load balancing proxy
can be used to balance the load without the need of decrypting traffic and without the need to store any keys.

We present an adapted C library to run an application in an enclave.
This C library does not implement a heavy-weight shielding mechanisms.
Our general approach is based on the observation that in many cloud-based applications
do not use the full OS functionality within an enclave: often application-level services instead of OS services are used.
For example, instead of storing files in the local file system, applications often use other services like an object store or a database.
% A less heavy-weight approach that does not include a complete library OS is sufficient.
We  show for the OS services that are used, we can use existing security libraries to protect the application. 
In particular, we show how to protect a database and the communication between the services of an application using existing libraries.

% A component might not need to run inside an enclave if all data processed by the component is encrypted,
% no keys are known to the component and data is protected against modifications by message authentication codes and no

% In this paper, we address the problem of how to run common cloud applications in secure enclaves and how to partition these applications into shielded and non-shielded components.

% Contributions

Summary of contributions:

\begin{enumerate}
\item We introduce a system and failure model that should guide us how  to build shielded applications for public clouds.
\item We demonstrate how to run a traditional three-tier web service securely in a public cloud environment.
\item We provide micro and macro benchmarks on the performance of our secured applications.
\end{enumerate}

% Outline

In what follows, we introduce the system and failure model in Section~\ref{sec:sysmodel} and briefly introduce SGX in  Section~\ref{sec:background}. 
We describe the design and architecture of our approach in Section~\ref{sec:design}, the implementation in Section~\ref{sec:implementation}
and evaluate our approach in Section~\ref{sec:evaluation}. The related work is described in Section~\ref{sec:rwork} 
% and we  discuss our design alternatives in Section~\ref{sec:discussion} 
and conclude with Section~\ref{sec:conclusions}.

\section{System and Failure Model}
\label{sec:sysmodel}

The system and failure model defines what a web application developer can assume when building a web application. 
In a nutshell, the system model says how the system behaves during normal operations and the failure model defines what can go wrong if the system is attacked:
a developer needs to expect and deal with these failures which can be caused by attacks or by benign faults like host crashes.  

% If a failure model defines weak failure assumptions (like all variables can be changed arbitrarily by attackers or failures), 
% it can become very difficult to build applications since developers need to worry about too many things that can go wrong.
% However, if the failure assumptions are strong, they might be violated in practice.
% Such a violation could be used by an attacker to access confidential data. 

\subsection{Observations}

Typical web applications consist of multiple services like a load balancer, a database and a set of application servers.
We call a web application {\em trustworthy} if it protects the confidentiality of its data.
To build a trustworthy web application, an application designer has to decide what are valid assumptions when running
the individual services. In the context of cryptoanalysis, one uses an attack model that describe the type of attacks permitted
to break a system. To simplify the construction of services, we use a declarative approach in which we  
state our assumptions in the system and failure model:  these assumptions are true under all permitted attacks.

% Our system and failure model is based on the following considerations.
As argued above, we cannot trust that a provider of a public cloud can protect the confidentiality of the data being processed and stored in the cloud.
% While the cloud provider might have the best intentions,
% it might have rogue cloud administrators on payroll or it might legally be required to give access to the data to government agencies.
% Also, a cloud consists of millions of lines of codes and hence, very like to contain vulnerabilities.
% Attackers might know and use some of the vulnerabilites in the cloud software to hijack cloud services and use these to attack or retrieve the data of applications running in the cloud.
%
% Virtual machines and containers are under the control of a hypervisor and/or host operating system.
% The operating system of a virtual machine is under the control of a cloud tenant.
An application designer should not place  trust in this operating system either.
The operating system could, for example, easily be attacked via the hypervisor.
%
% Even if the cloud provider protects the hypervisor and its cloud infrastructure against attacks,
% the operating systems of a VM might still be successfully attacked.
% Operating systems have become huge pieces of software consisting of millions lines of code.
% Operating systems will, in particular, contain bugs that can be used by attackers to compromise the applications running under the control of the operating system.
% Hence, the code running inside of an enclave cannot really trust any code running outside the enclave. 
%
%An application designer should, however, be able to trust the code running inside the enclave: this code should be completely under the control of the developer, i.e.,
%the all source of all components running inside the enclave is available for the application developer for inspection.
Programming applications in an environment where an application designer cannot trust anything except the code running inside individual enclaves is difficult.
%This might lead to a "paranoia programming style" in which a programmer tries to check all interactions and these checks might neither be complete nor correct.
One has to define at least some assumptions regarding the communicate between enclaves.
 
\subsection{System and Failure Assumptions}

Our approach to cope with programming applications is as follows.
We provide a simple and known system and failure model for application programmers. 
Software running inside of an enclave has the following {\em crash / performance} failure semantic:
\begin{description}
\item[Crash Failures:] The software running inside an enclave can crash.
When an enclave crashes, it loses all its in-memory state.

\item[Performance Failures:] The software running inside an enclave can be arbitrarily slow.
For example, since the operating system assigns too short or even no time slices to the threads running inside an enclave.
\end{description}

\noindent
No other failures are permitted, in particular,  our system model states that
the integrity and the confidentiality of the application state of programs running inside of enclaves is guaranteed, i.e., 
the state of the enclave cannot be modified or accessed by any entity except the code running in the enclave. 
\\

\noindent
The communication between enclaves  and between an enclave and a client has the following {\em omission / performance} failure semantics:
\begin{description}
  \item[Omission Failures:] Messages sent by one enclave to another enclave might be dropped by the network or the operating system or some other components on the path between the two enclaves.
  \item [Performance Failures:] Even very reasonable expectations about the transmission delays of messages might be violated.
\end{description}

\noindent
No other failures are permitted: consider a  message $m$ that is 
sent by a sender enclave $s$  to a destination enclave $d$. It is ensured that
message $m$ is delivered at most once to its destination $d$ (no duplicates),
a message $m$ received by $d$ from $s$  was indeed sent by $s$ to $d$ (no modifications and no spoofing), and
a  message $m$ can only be read by the destination $d$ (confidentiality).

% To ensure omission/performance failure semantics for the communication system, we need to ensure that we  

We give no guarantees regarding the storage of objects:
it needs to be expected that objects can be arbitrarily read and modified.
To ensure the confidentiality and integrity of objects,
an application would need to encrypt the content and protect it with message authentication codes.    

% \tk{If we need more text, we can add a paragraph about the usual assumptions, like unbreakable HMAC, tamper-resistant TPM, etc.}

\subsection{Implications}

A cloud provider could attack a cloud customer by slowing down or crashing the customer's services.
Performance variability due to the inherently shared nature of a public cloud is a well-known problem that cloud customers already face today.
To cope with performance variability - whether intentionally/malicious or random - cloud users extensively monitor their applications.
Prolonged and/or unusually large deviations from the expected performance will prompt the cloud customer to take action:
either restart the application to decrease interference from ``noisy neighbors'' or move to a different cloud provider altogether.
Hence, it is in the cloud provider's best interest to grant the agreed upon resources to each customer.

To guarantee the omission/performance semantics on the communication channel,
we default to encrypt the entire communication traffic between enclaves at the transport layer.
Encrypted communication is becoming commonplace on the internet.
However, once inside the data center,
the additional effort to encrypt traffic was deemed unnecessary as the environment is considered secure and the cloud provider is trusted.
Recently, this has changed and companies started to encrypt even traffic within a private data center.
Our solution is to also employ transport-layer encryption, i.e., SSL/TLS,
between the individual enclaves.
While SSL/TLS does not solve the performance failure problem,
it helps to detect when packets are dropped. In this case, the TLS connections is closed
and messages that are in transit will be dropped. 


%% Messages might indeed by modified, spoofed or replayed by an attacker.
%% We need to enforce the performance/omission failure semantics by detecting such messages.

%To simplify the design of new cloud-based applications and the adaptation of existing applications that should run inside of enclaves,
%we need to support simple failure semantics.
%Software running inside of an enclave needs to interact with components running outside of enclaves, like the underlying operating system.
%Enforcing crash / performance / omission failure semantics for components running outside of an enclave is difficult to ensure and
%requires most likely an approach like the one described in~\cite{baumann2014shielding}.
%In web applications, we rarely need the full functionality of the underlying operating system - we often use external services instead of using operating system services.
%For example, instead of storing persistent data in the file system, 
%we often store persistent data in a data base or a key/value store.
%Companies, like Amazon, split their web applications into small independent (micro-)services, such as object stores and aggregation services \cite{Newman2015}.
%Hence, our focus is on providing a simple and secure communication semantics for communicating between the services of an application. 

The individual services of a web application each need to guarantee crash / performance / omission failure semantics.
While for some services the extra effort to guarantee the desired failure semantics within a secure execution context is warranted,
for other services the failure semantics can be guaranteed without running them within an enclave.
For example, if an application only handles encrypted data, without ever decrypting it,
this application might not need to run in an enclave at all.
In web application context, for example, this argument applies to the load balancer.
The load balancer only ever relays encrypted traffic between the client and the application servers.
Consequently, we do not need to run it within an enclave (see  Section \ref{sec:load-balancer}).


\iffalse
\subsection{OLD}

We need to protect our certificates (i.e., private keys of our certificates), the encryption keys of the data base keys, data being processed and data at rest.

- Performance: we use load balancer to ensure performance. We monitor the performance - change cloud provide if the provider does not ensure sufficient performance.

- Rollback attacks: we detect this (we do not prevent this) by sampling. If we see a rollback, we change cloud provider ...
\fi

\section{Background: Secure Enclaves}
\label{sec:background}

% \subsection{Secure Execution Context}

Intel's Software Guard Extensions (SGX)~\cite{IntelSGXReference} allow to create secure executions contexts (\emph{enclaves}).
To this end, new instructions were added to the X86 instruction set. 
Specifically, the processor ensures that certain memory regions which we call \emph{enclave memory} 
can only be accessed from within the enclave.
Accesses to enclave memory from outside the enclave are prevented by the hardware and fail with an exception. More precisely,
an enclave consists of memory pages and these pages are stored in the enclave page cache (EPC). Each page in the EPC
is owned by at most one enclave and only this enclave is permitted to access this page. 


Enclave memory is encrypted and its integrity is protected by cryptographic means.
The startup procedure ensures that only code intended to run within the enclave is actually part of the enclave (using remote attestation).
Automatic encryption of any data that leaves the CPU's registers and caches disables a broad range of attacks.
In particular, the cloud provider must no longer be trusted.
Without SGX, it is possible for a malicious administrator to login into a cloud server as root and extract a memory image of any running virtual machine.
%This is no longer possible when Intel's processor extensions are used.
While this is still possible when using SGX, the attacker will not be able to learn any confidential data as the memory pages that belong to an enclave 
are both encrypted and accesses from outside of the enclave result in a page fault.

In general, the hard- and software trusted computing base is reduced to the processor and the software running within the enclave.
Previously, it included the entire cloud server's hardware,
as well as the software stack running ``below'' the user's application, including the operating system in the VM and the hypervisor.

Intel SGX is not a panacea:
%in the absence of directly accessing an applications memory,
even if enclave memory is protected against direct access of the rest of the software stack,
other possible attack venues still exist -- some old and some new.
For example, the protection offered by Intel SGX could still have been circumvented by an application layer bug like Heartbleed.
System designers will also have to reevaluate their assumptions about interfaces with potentially untrusted components.
This specifically includes the operating system,
which runs outside of the enclave and can no longer be trusted,
opening up a new class of attacks~\cite{checkoway2013iago}. Moreover, side-channel attacks 
remain possible when using SGX \cite{xu2015controlled-channel}.

The first version of SGX (SGXv1) has several limitations. 
Some of these limitations were identified in~\cite{baumann2014shielding} and will be addressed by the 2nd version, i.e., SGXv2~\cite{IntelSGXReference}. 
Independent of the version, the CPU has to track the status of all pages in the extended page cache (EPC), 
for example, the CPU needs to know which enclave owns a page.
This status is maintained in the enclave page cache map (EPCM) which is a secure data structure stored on the CPU. 
The current implementation of SGX limits the size of the EPCM  and hence, the size of the EPC to 128 MBytes for mobile Skylake processors. 
We expect that this limit will be vary for different CPU models and that it will increase over time.
\iffalse
\subsection{Reactive Programming Model}

Why do we explicitly target this kind of application?
It must somehow make our life easier to focus on this class of applications.

reactive programming (node.js, Vert.x, \ldots)

\begin{enumerate}
\item mention Ceu as a reactive programming language
\item mention that using an interpreted language pulls the entire interpreter into the TCB
\end{enumerate}

\subsection{Cryptographic Primitives}

\subsection{Adversary Model}
\fi

\input{arch.tex}
\input{impl.tex}
\input{eval.tex}
\input{rwork.tex}
\input{fwork.tex}
\input{conclusion.tex}

% \clearpage

\bibliographystyle{plain}
\bibliography{refs}

\end{document}
