\section{Implementation}
\label{sec:implementation}

\subsection{System Calls}

%We have to show that this is actually a worthwhile performance optimization.
%How costly is an enclave exit/entry cycle?
%What is the net benefit of our ``asynchronous'' system calls?

System calls are the boundary between user and kernel space.
User space programs typically do not have direct access to the hardware
but have to ask the operating system to allocate memory and perform I/O operations.
The interface between the OS and the user-space application is the system call interface.

In modern architectures, the prevalent way to issue a system call consists of placing the system call arguments
into predefined registers and executing a special system call instructions (\verb+syscall+).
This causes the processor to transition into kernel mode and the execution branches to a well-defined kernel entry point.
In the context of Intel SGX,
executing the \verb+syscall+ instruction within an enclave is forbidden and raises a processor exception.

Using the operating system services could be supported via an enclave exit and re-entry for every system call,
in addition to the user/kernel mode transition.
An alternative is to move away from the traditional synchronous system call interface towards exception-less system calls~\cite{soares2010flexsc}.
This FlexSC work has demonstrated potential performance benefits due to increased temporal and spatial locality.
With core specialization, distinct cores in an SMP system could be dedicated to handle system calls.
No mode switches are necessary,
leading to less cache pollution and fewer TLB\footnote{translation lookaside buffer} invalidations.

%% \begin{figure}
%% \includegraphics{figs/async-syscall-conceptual}
%% \caption{Illustration of the interaction between enclave and non-enclave mode to perform system calls.
%% Instead of exiting the enclave mode to execute a system call,
%% an entry into a shared memory page is added.
%% Non-enclave threads consume items from the shared page and post results back.}
%% \label{fig:async-syscall-conceptual}
%% \end{figure}

For our current work,
we revisited the idea of asynchronous system calls in the context of secure execution contexts.
Conceptually, issuing a system call is converted into sending and receiving messages.
Like in FlexSC, communication between the issuer and the executor of a system call happens via a shared memory region.
The enclave writes fixed-size entries into a \emph{system call page}.
Each entry consists of a status field, the system call number, the system call arguments, and a return value.
The status field indicates the state of a particular entry.
Either the entry is \emph{free}, \emph{used}, \emph{written}, or \emph{done}.

When issuing a new system call,
the code looks for a \emph{free} entry and marks it \emph{used} with an atomic store operation.
The entry is then populated with the system call number and arguments,
before its status field is atomically set to the \emph{written} state.
The system call thread scans the system call page for entries in the \emph{written} state.
The code actually performing the system call sets the status field to \emph{done},
once the system call returns.
The results, i.e., success or failure,
are reported by setting the corresponding fields of the system call entry.
The original issuer of the system call reads the \emph{done} entry and,
after copying the results,
sets the status field back to \emph{free}.
The entry is then available to issue another system call.

\subsection{System Calls Implementation Details}
\label{sec:syscalls}

It is uncommon for applications to directly perform system calls.
Instead, the system call invocation is hidden inside the standard C library,
which almost all applications are linked against.
By adapting the standard library, changes are local to the library and cover a majority of system calls.

The GNU C library \emph{glibc} is one such implementation of the standard C library.
However, it is rather complex and more difficult to modify than some of the alternatives,
including \emph{dietlibc}, \emph{musl}, and \emph{ulibc}.
We decided to use \emph{musl}~\footnote{\url{http://www.musl-libc.org}} as it offered a good tradeoff between compatibility, feature completeness, and modifiability.

Essentially, a system call is similar to an ordinary function call.
The arguments, including the system call number and parameters,
are placed into well-defined locations.
For example, the system call number on X86\_64 is placed into the EAX register.
Once the registers have been populated,
the \verb+syscall+ instruction is executed.
This causes the control flow to branch to a previously registered system call handler.


SGX, however, forbids to call the \verb+syscall+ instruction from within the enclave context.
Instead of leaving the enclave to perform a system call
- a potentially costly operation especially if done  frequently -
we let a second thread execute the system call for us.
In this way, the thread issuing the system call does not have to leave the enclave.

Some system calls are easy to forward,
as they take no arguments, for example, \verb+getpid+,
or only scalar arguments, for example, \verb+close+.
System calls passing pointers to memory are more complex. 
For example, a \verb+read+ system call expects a pointer to a writable memory region to be filled by the read operation.
However, we cannot pass a pointer to enclave memory as enclave memory is inaccessible outside of the enclave.


\begin{figure}
\centering
\includegraphics[width=\columnwidth]{figs/systemcall-hor}
\caption{Syscalls inside an enclave are provided via a variant of the musl C library. The syscall is executed
by a thread outside the enclave.  Arguments are copied to a shared syscall page. }
\label{fig:systemcall}
\end{figure}


Figure \ref{fig:systemcall} demonstrates how a \verb+read+ system call is executed.
The thread running inside of an enclave issues a call (step 1). 
The library wrapper allocates a new buffer outside of the enclave, and adds a record in the system call page (steps 2 and 3). 
The thread running in the untrusted environment detects a new entry and performs the system call using the provided arguments (step 4).
The buffer is filled by the read operation (step 5).
When the system call returns,
the data is copied from ``non-enclave memory'' to the original ``enclave memory'' buffer (step 6).
The same logic applies to buffers used for passing information to the OS, for example,
during a \verb+write+ system call.
Here, we must also allocate a buffer accessible from outside the enclave,
copy the data,
forward the call,
and free the temporary buffer once the system call returns.

A few system calls are even more complicated to handle,
because they require a ``deep copy''.
Because writing the system call wrappers manually is tedious and error prone,
we developed a tool to automate  the wrapper generation.
The tool parses an annotated list of system calls and generates the ``serialization'' code based on the description.

An example of an annotated system call is shown in Figure~\ref{fig:annotated-system-call}.
The previously mentioned \verb+write+ system call takes three arguments:
a file descriptor (\verb+fd+), a pointer of the data to be written (\verb+buf+),
and the size of data to be written (\verb+count+).
The \verb+fd+ and \verb+count+ parameters are scalars and can be copied easily.
The \verb+buf+ parameter is annotated with information on how it is accessed (\verb+R+ for read-only).
The annotation also indicates how many bytes of the pointer are safe to access.
In the case of the \verb+write+ system call,
the number of bytes is passed as the \verb+count+ parameter,
hence the annotation reads \verb+Size:count+.


\begin{figure}[b]
\begin{verbatim}
ssize_t syscall_SYS_write(
    int fd,
    const void* buf /* Attrs:R, Size:count */,
    size_t count
);
\end{verbatim}
\caption{Annotated system call.}
\label{fig:annotated-system-call}
\end{figure}

Using the annotated list of system calls,
we are able to proxy almost the full set of Linux system calls:
currently about 270 syscalls.
Right now, our implementation does not yet support 55 syscalls.
Some of these are legacy syscalls (for 32-bit architectures) and other syscalls do not make sense to support inside of an enclave.
For example, a \verb+fork+ inside an enclave is not supported since we cannot fork an enclave:
the OS would have to read the enclave's memory to create a copy of it,
which is forbidden according to the SGX specification.

% \tk{Which other system calls are not supported?}


\subsection{Memory handling}

% malloc
Typical POSIX applications manage their memory with the \verb+malloc+ and \verb+free+ family of functions. 
\verb+malloc+ and \verb+free+ are usually implemented in the C library,
which itself uses the \verb+mmap+ system call together with some meta-data structures. 
The \verb+mmap+ system call causes operating system to add pages with the specified protection (e.g., writable or executable) to the process's address space. 
This conflicts with the restrictions of the SGXv1 instruction set,
which does not allow to extend the enclave memory range. 
This makes transparent implementation of dynamic shielded memory management impossible,
as all the enclave memory must be pre-allocated.

% fixed memory allocator
To provide compatibility for applications that use dynamic memory management,
we use a memory allocator that uses a fixed region of enclave memory. 
Such allocators are, for example, used in resource-constrained embedded systems.
This makes unbounded dynamic memory allocation impractical. 
In our implementation we use the mem5 allocator from the SQLite library, 
which is exhaustively tested and has mathematically-proven properties \cite{Robson:1974:BFC:321832.321846}. 
Once initialized with a memory region,
the allocator operates without further \verb+mmap+ calls.

% mmap
As application may directly invoke \verb+mmap+ system calls,
it is necessary to intercept them. 
In case of anonymous memory requests,
we use \verb+malloc+ to allocate the requested block of memory. 
Calls to \verb+mmap+ may request memory with different protection flags,
but SGXv1 does not allow to change the protection flags of in-enclave memory. 
Therefore, the memory region used for malloc has all possible access types enabled.
The \verb+mprotect+ system call is unsupported.

\begin{figure*}
\includegraphics{figs/lua-results-2in-1in}
\includegraphics{figs/stud-results-2in-1in}
\includegraphics{figs/www-results-2in-1in}
\caption{Standard deviation is less than 5\% for all LUA benchmarks.}
\label{fig:eval-micro}
\end{figure*}

In case of memory mapping files,
we allocate memory outside of the enclave.
We argue that memory-mapped files live outside the enclave and the content originates from the operating system,
which is outside of the application's control.

%munmap
The \verb+munmap+ system call correctly distinguishes between memory-mapped files and anonymous memory that was retrieved with \verb+malloc+. 
To achieve this, we utilize the knowledge that all anonymous memory comes from a fixed memory range,
and after a trivial address interval check, we can choose the correct function.
Partial \verb+munmap+ of an address range is unsupported.

% sgxv2
The restrictions presented above are removed in the SGXv2 instruction set. 
Once this extension becomes more widespread,
we will revisit our design decision regarding the memory management. % merged with non-protected memory allocation code.

\subsection{Threading} 
Intel SGX allows multiple threads to enter an enclave simultaneously. For each
such thread a Thread Control Structure (TCS) is maintained, which contains
necessary thread-specific metadata. 

Applications running inside an enclave cannot rely on the synchronization and signaling primitives provided by the operating system,
as it may violate correctness.
For instance, two threads may enter the same critical section simultaneously.
The OS can also prevent some (or all) threads from being scheduled,
corresponding to a denial-of-service attack.

We use an M:N threading model:
we start N native kernel threads and perform user-level scheduling of M enclave threads running on top of them using the lthread\footnote{\url{https://github.com/halayli/lthread}} library.
Lthread is based on co-routines and is able to leverage several kernel threads.
We implemented a pthread-compatible API on top of the lthread library to support programs that use pthreads.

\subsection{Enclave Emulation}
\label{sec:enclave-emulation}

Since we do not yet have access to real SGX hardware
\footnote{Our SGX-enabled hardware is expected to arrive just after the Eurosys submission deadline. 
In case this paper is accepted,
we would like to update our performance measurements for executing on real SGX hardware instead.},
we aimed to faithfully emulate the most salient points of the new security extensions.
One important aspect to run commodity Linux application on top of SGX is the handling of system calls.
We described in detail (cf. Section~\ref{sec:syscalls}) how we proxy system calls between the OS and the enclave.

Our enclave binary consists of a single statically linked shared object file \verb+app.so+.
Within the shared object file, there exists a standard \verb+main()+ function,
which represents the application-level entry point.
Applications are started with a special ``loader'' application.

Most importantly, the loader allocates memory to be used by the enclave.
This emulates the ``enclave memory'', as the enclave is supposed to manage its memory independently of the operating system.
According to the first version of SGX,
the allocation is fixed for the lifetime of the enclave.
In SGXv2, it will be possible to dynamically add and remove memory from the enclave.
We plan to add this feature in future versions of our framework.

% \tk{Any other special features/tricks worth mentioning? Changed object file for? ELF?}

%\subsection{Building the Application Stack}

%\begin{figure}
%\begin{center}
%\includegraphics[width=0.4\textwidth]{figs/libraries}
%\end{center}
%\caption{All code running within an enclave is statically linked. We provide a modified C library (sgxmusl) to allow syscalls
%inside of enclaves. The protection of communication links is ensured with the help of openssl. The confidentiality and integrity
%of the data is ensured with the help of sqlcipher.}
%\label{fig:build-chain}
%\end{figure}

%As we mentioned above, all code that runs in enclaves is statically linked (see Figure \ref{fig:build-chain}). To do so, we need to compile
%all  libraries and the application and then statically link these. This means that we do not only need to trust all libraries that we link against but
%also all tools (like compiler, linker, etc) that are used to generate the code that runs inside the enclaves. Hence, developers should have
%access not only to the source code of all applications that run in enclaves but also the libraries and preferably, the tool chain.  In principle, developers have in this way
%the opportunity not only to check the security of all code but in particular, also to fix vulnerabilities that were found.

%When building applications, we assume that the developer machines are trusted. Ideally, the generation of the application code is actually also performed
%inside of enclaves. In this case, the developer machines could actually be machines in a public cloud. 

% {\color{red} reference ``On Trusting Trust'' and mention the recent subversion of Apple's app store.
% The TCB also includes the entire tool chain, e.g., compiler, linker, etc., which is used to build the secure application.}
