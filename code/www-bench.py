#!/usr/bin/env python2.7

import os.path
import shlex
import subprocess as sp
import sys
import time

root = sys.argv[1]

def main() :

    cwd = os.getcwd()
    os.chdir('/tmp')
    sp.check_call(shlex.split('dd if=/dev/urandom of=16KiB.txt count=1 bs=16K'))
    sp.check_call(shlex.split('dd if=/dev/urandom of=1MiB.txt count=1 bs=1M'))
    
    for type in ['native', 'sgx'] :
        for protocol in ['http', 'https'] :
            for url_path in ['16KiB.txt', '1MiB.txt'] :
                proc_srv = start_web_srv(type, protocol)
                time.sleep(1)
                start_ab(url_path, type, protocol)
                proc_srv.kill()

    os.chdir(cwd)

def start_web_srv(type, protocol) :
    dir = os.path.join(root, 'mongoose-'+type, 'examples/simplest_web_server')
    exe = {'native' : 'simplest_web_server',
           'sgx' : 'starter %s' % (os.path.join(root, 'mongoose-sgx', 'examples', 'simplest_web_server', 'simplest_web_server.so')) }
    params = {'http' : '',
              'https' : '--ssl %s' % (os.path.join(root, 'sgx-musl-tests/keypair-1024.pem'))}
    cmd = os.path.join(dir, exe[type]) + ' ' + params[protocol]
    print cmd
    return sp.Popen(shlex.split(cmd))

def start_ab(url_path, type, protocol) :
    nr_requests = 1000000
    
    cmd = 'ab -t 30 -n %d -c 1 %s://127.0.0.1:8443/%s' % (nr_requests, protocol, url_path)
    print cmd
    out = sp.check_output(shlex.split(cmd))
    
    for line in out.split('\n') :
        if line.startswith('Requests per second') :
            print line

if __name__ == "__main__" :
    main()
