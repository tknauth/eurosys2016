#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

int main(int argc, char *argv[])
{
    size_t i;
    pid_t p;
    size_t max_iter;
    struct timespec start, end, delta;

    if (argc < 2) return 1;

    max_iter = atoi(argv[1]);

    clock_gettime(CLOCK_MONOTONIC, &start);
    for (i = 0; i < max_iter; i++) {
        p = getppid();
        if (p == 0) return 1;
    }

    clock_gettime(CLOCK_MONOTONIC, &end);
    delta = diff(start, end);
    printf("%ld.%ld\n", delta.tv_sec, delta.tv_nsec);

    return 0;
}
