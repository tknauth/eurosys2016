#!/usr/bash

make CC=$1/musl-installed/bin/musl-gcc
make CC=$1/musl-installed/bin/musl-gcc SGXCC=$1/musl-sgx-internal/bin/musl-gcc

I=100000
for P in getpid read write ; do
    echo -n "musl $P "
    ./$P $I
    echo -n "sgxmusl $P "
    ./starter ./$P.so $I
done
