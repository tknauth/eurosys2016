#include <assert.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <unistd.h>

struct timespec diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    return temp;
}

int main(int argc, char *argv[])
{
    int fd;
    size_t i;
    char buf[4096];
    size_t ln = 4096;
    size_t max_iter;
    size_t ret;
    struct timespec start, end, delta;

    if (argc < 2) return 1;
    max_iter = atoi(argv[1]);

    fd = open("/dev/null", O_RDWR);

    assert(fd != -1);

    clock_gettime(CLOCK_MONOTONIC, &start);
    for (i = 0; i < max_iter; i++) {
        ret = write(fd, buf, ln);
        assert(ret == ln);
    }
    clock_gettime(CLOCK_MONOTONIC, &end);

    delta = diff(start, end);
    printf("%ld.%ld\n", delta.tv_sec, delta.tv_nsec);

    return 0;
}
