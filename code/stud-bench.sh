#!/bin/bash

# set -x

ROOT="$1"

KEYSIZES="1024 2048 4096"

netcat -k -l 8000 > /dev/null &

pushd "$ROOT"
pushd stud-native
for keysize in $KEYSIZES ; do
    ./stud ./keypair-"$keysize".pem --ssl 2>/dev/null &
    sleep 2
    $ROOT/openssl-1.0.2d-native/apps/openssl s_time -connect 127.0.0.1:8443 -cipher ECDHE-RSA-AES256-GCM-SHA384 -new -time 5
    killall -u $USER stud
done
popd

pushd stud-sgx
for keysize in $KEYSIZES ; do
    ./stud-starter ./keypair-"$keysize".pem 2>/dev/null --ssl &
    sleep 2
    $ROOT/openssl-1.0.2d-native/apps/openssl s_time -connect 127.0.0.1:8443 -cipher ECDHE-RSA-AES256-GCM-SHA384 -new -time 5
    killall -u $USER stud-starter
done
popd
popd

killall -u $USER netcat
